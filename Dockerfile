FROM rootproject/root:6.22.02-centos7

## ensure locale is set during build for ubuntu
## ENV LANG C.UTF-8
## for centos
# -i: specify the locale definition file
# -f: specify the character set
## RUN localedef -i en_US -f UTF-8 en_US.UTF-8
## the above already happens in the root container

ARG DEBIAN_FRONTEND=noninteractive

RUN yum makecache
RUN yum update -y

RUN yum install -y python3-pip && \
    yum install -y git debconf-utils && \
    echo "krb5-config krb5-config/add_servers_realm string CERN.CH" | debconf-set-selections && \
    echo "krb5-config krb5-config/default_realm string CERN.CH" | debconf-set-selections && \
    yum install -y krb5-workstation krb5-libs krb5-auth-dialog && \
    yum install -y vim less screen graphviz python3-tk wget && \
    yum install -y jq tree hdf5-tools bash-completion

RUN pip3 install --upgrade pip

RUN pip3 install setuptools wheel
RUN pip3 install setuptools-rust docker-compose

RUN pip3 install numpy && \
    pip3 install Keras==2.3.1 && \
    pip3 install Keras-Applications && \
    pip3 install Keras-Preprocessing && \
    pip3 install uproot && \
    pip3 install uproot-methods && \
    pip3 install pyroot && \
    pip3 install jupyter && \
    pip3 install jupyterhub && \
    pip3 install jupyterlab && \
    pip3 install matplotlib && \
    pip3 install Markdown && \
    pip3 install seaborn && \
    pip3 install hep_ml && \
    pip3 install sklearn && \
    pip3 install tables && \
    pip3 install tdqm && \
    pip3 install pypi && \
    pip3 install mplhep && \
    pip3 install papermill pydot Pillow
