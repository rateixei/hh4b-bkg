# ML-Container for HH4b background reweighting

to run this image with singularity run the command:
```
singularity exec --nv docker://gitlab-registry.cern.ch/rateixei/hh4b-bkg/hh4b-bkg:latest bash
```

If you want to extend the image you can simply fork this repository and add additional packages in the [`Dockerfile`](https://gitlab.cern.ch/rateixei/hh4b-bkg/-/blob/master/Dockerfile) and possibly create a merge request.
With the flag `-B` you can add other directories to the container of needed:

```
singularity exec -B ${PWD}:/mnt --nv docker://gitlab-registry.cern.ch/rateixei/hh4b-bkg/hh4b-bkg:latest bash
```

Don't forget to clean your cache every once in a while:

```
singularity cache clean
```
